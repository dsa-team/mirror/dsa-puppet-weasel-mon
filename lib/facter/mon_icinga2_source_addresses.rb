# Learn our source IP address for when we contact the icinga2 master host
Facter.add(:mon_icinga2_source_addresses) do
  confine kernel: :linux

  setcode do
    require 'json'
    require 'resolv'
    mon_fact = Facter.value(:mon)
    if mon_fact
      masters = mon_fact.dig('misc', 'icinga2_masters') or []
      srcs = masters.map { |server| Resolv.getaddresses(server) }.flatten().uniq().map do |addr|
        iprouteoutput = IO.popen(['ip', '-json', 'route', 'get', addr], :err => "/dev/null" ).read()
        begin
          iprouteparsed = JSON.parse(iprouteoutput)
          iprouteparsed.dig(0, 'prefsrc')
        rescue JSON::ParserError # ancient ip?
          m = iprouteoutput.match(/[[:space:]]+src[[:space:]]+([0-9.:a-f]*)[[:space:]]/)
          if m
            m[1]
          end
        end
      end
      srcs.reject{ |s| s.nil? }.uniq()
    end
  end
end
