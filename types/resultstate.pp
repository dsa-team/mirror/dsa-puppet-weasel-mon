# Some checks allow you to configure the result
# of the check given certain conditions.
#
# This type alias lists the various allowable states.
type Mon::Resultstate = Enum['ok', 'warning', 'critical', 'unknown']
