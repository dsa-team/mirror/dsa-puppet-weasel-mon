# A strict type for ranges, cf https://www.monitoring-plugins.org/doc/guidelines.html#THRESHOLDFORMAT
type Mon::Monitoringrange = Variant[Numeric, Pattern[
  /\A@?((~|-?[\d]*(\.\d*)?):)\z/,                   # just a start and no end (i.e. end at infinity)
  /\A@?((~|-?[\d]*(\.\d*)?):)?-?([\d]*(\.\d*)?)\z/, # optional start and an end
]]
