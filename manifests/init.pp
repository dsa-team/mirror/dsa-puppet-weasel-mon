# Set base variables and configure things common to all devices that install an
# icinga agent, satellite, or master.
#
# @param icinga2_masters
#   List of hosts that are icinga2 masters (one or two)
# @param icinga2_parent_zone
#   The parent zone (unless this node is a master already)
# @param icinga2_icinga_ca_host_fingerprint
#   The fingerprint of the host ssl certificate on the primary for passing
#   on to icinga2::feature::api.
# @param icinga2_ssl_ticket_salt
#   Ticket salt for authenticating ssl certificate signing requests
#
# @param icinga2_ido_db_password
#   Password for the DB user for the IDO DB
#   Only needed on the master
# @param icinga2_icingaweb_api_password
#   Password for icingaweb's API access.
#   Only needed on the master
# @param icinga2_icingaweb_roles
#   Dictionary of icingaweb roles with their privileges.
#   Cf. icingaweb2::config::role
#   Only needed on the master
#
# @param firewall
#   The type of firewalling to use for icinga api access etc
#
# @param icinga2_services
#   List of service checks to instantiate on this system.  Key from check class
#   to a hash of check name and parameters.
# @param icinga2_checkcommands
#   Data to create a set of icinga2::object::checkcommand instances iff we are
#   the primary master.  For each check, the mon_meta hash key is removed, if it
#   exists.
# @param monitoring_home
#   Directory where check scripts are installed to
# @param icinga2_agent_connects_to_parent
#   Whether this icinga agent connects to its parent.  If not, then the parent will
#   attempt to connect to this agent.
#
# @param checkcommand_classname_prefixes
#   List of puppet class name prefixes where check commands that we want to
#   install via mon::install_command live.
#   This module (mon) is always implicitly prepended to the list.
# @param checkcommand_sources
#   List of puppet source prefixes where check commands that we want to install
#   via mon::install_command live.
#   This module's files/commands is always implicitly prepended to the list.
#
# @param default_check_interval
#   A default value for all service checks.  cf. icinga2::object::service
# @param default_retry_interval
#   A default value for all service checks.  cf. icinga2::object::service
# @param default_max_check_attempts
#   A default value for all service checks.  cf. icinga2::object::service
# @param default_host_check_interval
#   A default value for all host checks.  cf. icinga2::object::host
# @param default_host_retry_interval
#   A default value for all host checks.  cf. icinga2::object::host
# @param default_host_max_check_attempts
#   A default value for all host checks.  cf. icinga2::object::host
class mon (
  Array[Stdlib::Host, 1, 2]     $icinga2_masters,
  Stdlib::Host                  $icinga2_parent_zone = 'master',
  Optional[String]              $icinga2_icinga_ca_host_fingerprint = undef,
  Boolean                       $icinga2_agent_connects_to_parent = true,

  Optional[String] $icinga2_ssl_ticket_salt = undef,
  Optional[String] $icinga2_ido_db_password = undef,
  Optional[String] $icinga2_icingaweb_api_password = undef,
  Hash[String, Hash[String,String]] $icinga2_icingaweb_roles = {},

  Optional[Enum['ferm', 'nft']] $firewall = undef,

  Hash[String, Hash] $icinga2_services = {},
  Hash[String, Hash] $icinga2_checkcommands = {},
  Stdlib::AbsolutePath $monitoring_home = '/opt/monitoring',

  Array[String] $checkcommand_classname_prefixes = [],
  Array[String] $checkcommand_sources = [],

  Optional[Icinga2::Interval] $default_check_interval = undef,
  Optional[Icinga2::Interval] $default_retry_interval = undef,
  Optional[Integer[1]]        $default_max_check_attempts = undef,

  Optional[Icinga2::Interval] $default_host_check_interval = undef,
  Optional[Icinga2::Interval] $default_host_retry_interval = undef,
  Optional[Integer[1]]        $default_host_max_check_attempts = undef,
) {
  $icinga2_is_master = $trusted['certname'] in $icinga2_masters
  $icinga2_is_primary = $trusted['certname'] == $icinga2_masters[0] # sets up master zone
  if ($icinga2_is_master and $icinga2_parent_zone != 'master') {
    fail('master nodes should not change parent_zone name away from master.')
  }
  $this_zone_name = $icinga2_is_master ? {
    true    => 'master',
    default => $trusted['certname'],
  }

  $name_munged = $name.regsubst('::', '/', 'G')
  $all_checkcommand_classname_prefixes = [ "${name}::check_commands" ] + $checkcommand_classname_prefixes
  $all_checkcommand_sources            = [ "puppet:///modules/${name_munged}/commands" ] + $checkcommand_sources

  class{ 'mon::icinga2':
  }

  ensure_packages(['monitoring-plugins', 'monitoring-plugins-contrib'])

  file { [
    $mon::monitoring_home,
    "${mon::monitoring_home}/bin",
    "${mon::monitoring_home}/etc",
    ]:
    ensure  => directory,
    force   => true,
    recurse => true,
    purge   => true,
    owner   => 'root',
    group   => 'root',
    mode    => '0755'
  }

  if $icinga2_is_primary {
    $icinga2_checkcommands.each |$check_name, $data| {
      $filtered_data = $data.filter |$key, $val| { $key != 'mon_meta' }

      unless $filtered_data.empty() {
        mon::icinga2::ensure::checkcommand { $check_name :
          * => $filtered_data,
        }
      }
    }
    Mon::Icinga2::Ensure::Checkcommand <<| |>>
  }

  include mon::services

  $icinga2_services.each|$service_type, $service_definitions| {
    if defined("mon::${service_type}") {
      create_resources("mon::${service_type}", $service_definitions)
    } else {
      fail("Check type mon::${service_type} not found")
    }
  }

  # install requested check commands
  Mon::Install_command <<| tag == "worker--${trusted['certname']}" |>>
}
