# install ruby for the soas check
# the check script itself is part of monitoring-plugins-contrib
class mon::check_commands::soas (
) {
  ensure_packages(['ruby'])
}
