# Export a host definition to the icinga system
# @param host_name Name of this icinga host
# @param addresses List of addresses for this host.
#                  We will pick the first address of each type as a default for address and address6.
# @param address   IPv4 address of the host
# @param address6  IPv6 address of the host
# @param vars      Variables (and their values) to set for this object
#
# @param command_endpoint    Endpoint to run this host's check from, if not the master
# @param check_interval      cf. icinga2::object::host
# @param retry_interval      cf. icinga2::object::host
# @param max_check_attempts  cf. icinga2::object::host
define mon::icinga2::object::host (
  Stdlib::Host $host_name = $title,
  Array[Stdlib::IP::Address::Nosubnet] $addresses = pick($facts['mon_icinga2_source_addresses'], [ $facts['ipaddress'], $facts['ipaddress6'] ].delete_undef_values() ),
  Optional[Stdlib::IP::Address::V4::Nosubnet] $address  = ($addresses.filter |$a| { $a !~ Stdlib::IP::Address::V6 })[0],
  Optional[Stdlib::IP::Address::V6::Nosubnet] $address6 = ($addresses.filter |$a| { $a =~ Stdlib::IP::Address::V6 })[0],
  Optional[Icinga2::CustomAttributes] $vars = undef,

  Optional[Stdlib::Host]      $command_endpoint = undef,
  Optional[Icinga2::Interval] $check_interval = undef,
  Optional[Icinga2::Interval] $retry_interval = undef,
  Optional[Integer[1]]        $max_check_attempts = undef,
) {
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }
  $icinga_conf = @(EOF)
    <%- |
      String $title,
      Array  $imports = [],
      Hash   $parameters = {},
    | -%>
    # Mon::Icinga2::Object::Host[<%= $title %>]
    object Host "<%= $title %>" {
    <%- $imports.each |$import| { -%>
      import "<%= $i %>"
    <%- } -%>
    <%= icinga2::parse($parameters, 2) -%>
    }
    | EOF

  @@concat::fragment { "${host_name}::${title}":
    tag     => 'mon::icinga2::object',
    target  => "/etc/icinga2/zones.d/master/${host_name}.conf",
    order   => 40,

    content => inline_epp($icinga_conf, {
      title      => $title,
      parameters => {
        address            => $address,
        address6           => $address6,
        check_command      => 'hostalive',
        vars               => $vars,
        command_endpoint   => $command_endpoint,

        check_interval     => [$check_interval    , $mon::default_host_check_interval     ].reduce |$memo, $value| { [$memo, $value][Integer($memo =~ Undef)] },
        retry_interval     => [$retry_interval    , $mon::default_host_retry_interval     ].reduce |$memo, $value| { [$memo, $value][Integer($memo =~ Undef)] },
        max_check_attempts => [$max_check_attempts, $mon::default_host_max_check_attempts ].reduce |$memo, $value| { [$memo, $value][Integer($memo =~ Undef)] },
      }.delete_undef_values(),
    }),
  }
}
