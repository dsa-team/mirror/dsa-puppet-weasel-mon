# Export a service definition to the icinga system
# @param service_name  Name for this service
# @param display_name  A short description for this service
# @param check_command Icinga check command for this service
# @param on_target     Run the service check locally on the target host that is being checked
# @param worker        Endpoint to run this service's check from, if not from the host itself
# @param host_name     Icinga host that this service belongs to
# @param vars          Variables (and their values) to set for this object
# @param check_interval      cf. icinga2::object::service
# @param retry_interval      cf. icinga2::object::service
# @param max_check_attempts  cf. icinga2::object::service
define mon::icinga2::object::service (
  String                              $check_command = $title,
  String                              $service_name  = $title,
  Optional[String]                    $display_name  = undef,
  Boolean                             $on_target     = true,
  Optional[Stdlib::Host]              $worker        = if $on_target { $trusted['certname'] } else { undef },
  Stdlib::Host                        $host_name     = $trusted['certname'],
  Optional[Icinga2::CustomAttributes] $vars          = undef,

  Optional[Icinga2::Interval]         $check_interval = undef,
  Optional[Icinga2::Interval]         $retry_interval = undef,
  Optional[Integer[1]]                $max_check_attempts = undef,
) {
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }
  $icinga_conf = @(EOF)
    <%- |
      String $title,
      String $service_name,
      Array  $imports = [],
      Hash   $parameters = {},
    | -%>
    # Mon::Icinga2::Object::Service[<%= $title %>]
    object Service "<%= $service_name %>" {
    <%- $imports.each |$import| { -%>
      import "<%= $i %>"
    <%- } -%>
    <%= icinga2::parse($parameters, 2) -%>
    }
    | EOF

  @@concat::fragment { "${host_name}::${title}":
    tag     => 'mon::icinga2::object',
    target  => "/etc/icinga2/zones.d/master/${host_name}.conf",
    order   => 75,

    content => inline_epp($icinga_conf, {
      title        => $title,
      service_name => $service_name,
      parameters   => {
        display_name       => $display_name,
        check_command      => $check_command,
        host_name          => $host_name,
        command_endpoint   => $worker,
        vars               => $vars,

        check_interval     => [$check_interval    , $mon::default_check_interval     ].reduce |$memo, $value| { [$memo, $value][Integer($memo =~ Undef)] },
        retry_interval     => [$retry_interval    , $mon::default_retry_interval     ].reduce |$memo, $value| { [$memo, $value][Integer($memo =~ Undef)] },
        max_check_attempts => [$max_check_attempts, $mon::default_max_check_attempts ].reduce |$memo, $value| { [$memo, $value][Integer($memo =~ Undef)] },
      }.delete_undef_values(),
    }),
  }
}
