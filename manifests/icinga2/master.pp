# Configure icinga2 server
class mon::icinga2::master (
) {
  assert_private()
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  class{'mon::icinga2::master::icinga2': }
  class{'mon::icinga2::master::icingaweb2':
    ido_db_password        => $mon::icinga2_ido_db_password,
    icingaweb_api_password => $mon::icinga2_icingaweb_api_password,
  }

  $config_objects = puppetdb_query(
    [ 'from',
      'resources',
      [ 'and',
        ['=', 'type', 'Concat::Fragment'],
        ['=', 'tag', 'mon::icinga2::object'],
        ['=', 'exported', true],
      ],
      ['order_by', ['certname', 'title']],
    ])
  $icinga_conf = @(EOF)
    <%- |
      Array[Hash] $config_objects,
    | -%>
    <%- $config_objects.each |$item| { -%>
    # <%= $item['type'] %>[<%= $item['title'] %>]
    <%= $item['parameters']['content'] -%>
    <%- } -%>
    | EOF
  $by_target = $config_objects.group_by |$o| { $o['parameters']['target'] }
  $by_target.each |$target, $objects| {
    file { $target:
      tag     => 'icinga2::config::file',
      owner   => 'root',
      group   => $icinga2::globals::group,
      mode    => '0440',
      content => inline_epp($icinga_conf, {
        config_objects  => $objects,
      }),
    }
  }
  #Icinga2::Object::Host    <<| tag == 'mon::icinga2::object::host'    |>>
  #Icinga2::Object::Service <<| tag == 'mon::icinga2::object::service' |>>

  file { [
    '/etc/icinga2/zones.d/master',
    '/etc/icinga2/zones.d/global-templates',
    ]:
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    force   => true,
    recurse => true,
    purge   => true,
  }
}
