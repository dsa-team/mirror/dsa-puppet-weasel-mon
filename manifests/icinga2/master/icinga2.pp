# Configure icinga2 server
class mon::icinga2::master::icinga2 (
) {
  assert_private()
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  class { 'icinga2':
    constants => {
      'ZoneName'   => $mon::this_zone_name,
      'TicketSalt' => $mon::icinga2_ssl_ticket_salt,
    },
  }
  include icinga2::pki::ca

  class { 'icinga2::feature::api':
    pki             => 'none',
    accept_commands => true,
    bind_host       => '::',
    endpoints       => {},
    zones           => {},
  }
}
