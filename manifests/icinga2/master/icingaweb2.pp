# Configure icinga2 server
#
# @param ido_db_password         See mon::icinga2
# @param icingaweb_api_password  See mon::icinga2
class mon::icinga2::master::icingaweb2 (
  String $ido_db_password,
  String $icingaweb_api_password,
) {
  assert_private()
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  $ido_db_name = 'icinga2'
  $ido_db_user = 'icinga2'

  $icingaweb_api_user = 'icingaweb2'

  #######################
  ## Set up ido
  ######################
  if defined('postgresql::postgresql_password') {
    $pg_pass = postgresql::postgresql_password($ido_db_user, $ido_db_password)
  } else {
    $pg_pass = postgresql_password($ido_db_user, $ido_db_password)
  }
  class{ 'postgresql::server':
  }
  -> postgresql::server::db { $ido_db_name:
    user     => $ido_db_user,
    password => $pg_pass,
  }

  class{ 'icinga2::feature::idopgsql':
    user          => $ido_db_user,
    password      => $ido_db_password,
    database      => $ido_db_name,
    import_schema => true,
    require       => Postgresql::Server::Db[$ido_db_name],
  }

  icinga2::object::apiuser { $icingaweb_api_user:
    password    => $icingaweb_api_password,
    permissions => [ 'status/query', 'actions/*', 'objects/modify/*', 'objects/query/*' ],
    target      => '/etc/icinga2/conf.d/api-users.conf',
  }

  #######################
  ## Set up icingaweb
  ######################
  class { 'icingaweb2':
  }
  icingaweb2::config::authmethod { 'external':
    backend =>  'external',
  }
  ensure_resources( 'icingaweb2::config::role', $mon::icinga2_icingaweb_roles)
  class {'icingaweb2::module::monitoring':
    ido_host          => 'localhost',
    ido_type          => 'pgsql',
    ido_port          => 5432,
    ido_db_name       => $ido_db_name,
    ido_db_username   => $ido_db_user,
    ido_db_password   => $ido_db_password,
    commandtransports => {
      icinga2 => {
        transport => 'api',
        username  => $icingaweb_api_user,
        password  => $icingaweb_api_password,
      }
    }
  }
  ensure_packages([
  'php-curl',
  'php-pgsql',
  ],
  {
    notify => Service['apache2'],
  })
}
