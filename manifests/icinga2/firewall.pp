# Set up zones and endpoints config on all icinga2 hosts.
class mon::icinga2::firewall (
) {
  assert_private()
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  $my_addresses = $facts['mon_icinga2_source_addresses']
  # Set up firewalling
  case $mon::firewall {
    'nft': {
      nft::chain{ 'icinga2api': }
      nft::simple{ 'allow-icinga2api': dport => 5665, action => 'jump icinga2api' }

      unless ($mon::icinga2_is_primary) {
        if $mon::icinga2_parent_zone == 'master' {
          $nft_export_tags = $mon::icinga2_masters.map |$h| { "to-${h}" }
        } else {
          $nft_export_tags = [ "to-${mon::icinga2_parent_zone}" ]
        }
        if $my_addresses {
          @@nft::simple{ "icinga2-access-${trusted['certname']}":
            tag   => $nft_export_tags,
            saddr => $my_addresses,
            chain => 'icinga2api',
          }
        }
      }
    }
    'ferm': {
      ferm::rule::chain { 'icinga2api':
        description => 'chain for icinga2api access',
      }
      ferm::rule::simple { 'allow-icinga2api':
        port   => 5665,
        target => 'icinga2api',
      }
      Ferm::Rule::Simple <<| tag == "mon::icinga2::hierarchy::icinga2api-firewall::to-${trusted['certname']}" |>>


      unless ($mon::icinga2_is_primary) {
        if $mon::icinga2_parent_zone == 'master' {
          $nft_export_tags = $mon::icinga2_masters.map |$h| { "mon::icinga2::hierarchy::icinga2api-firewall::to-${h}" }
        } else {
          $nft_export_tags = [ "mon::icinga2::hierarchy::icinga2api-firewall::to-${mon::icinga2_parent_zone}" ]
        }
        if $my_addresses {
          @@ferm::rule::simple{ "icinga2-access-${trusted['certname']}":
            tag   => $nft_export_tags,
            saddr => $my_addresses,
            chain => 'icinga2api',
          }
        }
      }
    }
    default: {}
  }
}
