# Configure icinga2 agent
#
class mon::icinga2::agent (
) {
  assert_private()
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }


  # /etc/icinga2 is owned by the icinga2 module, and we do want to create
  # a file in that tree *before* the module comes and installs icinga2
  # so we can prevent mail being sent out before icinga2 is configured.
  exec { 'icinga2 prevent package mailing people directly after installing':
    command => @(EOF),
      sh -c "mkdir -p /etc/icinga2/conf.d && touch /etc/icinga2/conf.d/notifications.conf"
      | EOF
    path    => $facts['path'],
    creates => '/etc/icinga2',
  }
  -> class { 'icinga2':
    confd    => false,
    features => ['checker','mainlog'],
  }
  class { 'icinga2::feature::api':
    accept_config   => true,
    accept_commands => true,
    pki             => 'icinga2',
    ca_host         => $mon::icinga2_masters[0],
    ticket_salt     => $mon::icinga2_ssl_ticket_salt,
    fingerprint     => $mon::icinga2_icinga_ca_host_fingerprint,
    endpoints       => {},
    zones           => {},
  }
}
