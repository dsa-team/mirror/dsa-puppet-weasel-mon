# Configure endpoints and zones recursively
#
# This defined type adds the icinga endpoint and zone for the exporting host
# and collects the endpoints and zones for all its children, grandchildren,
# etc.
#
# @param node_endpoint_data   The properties of the endpoint to create for the exporting node
# @param node_zone_data       The properties of the zone to create for the exporting node

# @param this_endpoint_data   The properties of the endpoint to create for the collecting node, to export to its children in turn
# @param this_zone_data       The properties of the zone to create for the collecting node, to export to its children in turn

# @param node_agent_connects_to_parent
#   Whether this node connects to its parent (or its parent to the node).
#   Set from node's $mon::icinga2_agent_connects_to_parent.
# @param node_parent_zone     The name of the parent zone for the exporting node.  We collect on this.
define mon::icinga2::hierarchy::node_and_subtree_exported(
  Hash $node_endpoint_data,
  Hash $node_zone_data,

  Hash $this_endpoint_data,
  Hash $this_zone_data,

  Boolean $node_agent_connects_to_parent = $mon::icinga2_agent_connects_to_parent,
  Optional[String] $node_parent_zone = $node_zone_data['parent'],
) {
  assert_private()
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  if $node_parent_zone != $node_zone_data['parent'] {
    fail("Confused.  Node parent zone mismatch (${node_parent_zone} vs. ${node_zone_data['parent']}).")
  }

  $node_endpoint_name  = $node_endpoint_data['endpoint_name']
  $this_endpoint_name  = $this_endpoint_data['endpoint_name']
  $node_zone_name      = $node_zone_data['zone_name']
  $this_zone_name      = $this_zone_data['zone_name']

  if $node_endpoint_name !~ String { fail('Did not get a node_endpoint_name (or it is the wrong type)') }
  if $this_endpoint_name !~ String { fail('Did not get a this_endpoint_name (or it is the wrong type)') }
  if $node_zone_name !~ String { fail('Did not get a node_zone_name (or it is the wrong type)') }
  if $this_zone_name !~ String { fail('Did not get a this_zone_name (or it is the wrong type)') }
  if ($node_endpoint_name == $this_endpoint_name) != ($node_zone_name == $this_zone_name) {
    fail("Mismatch: if endpoint names match (${node_endpoint_name}, ${this_endpoint_name}) then so must zone names (${node_zone_name}, ${this_zone_name}) (and vice versa)")
  }

  # Only include the host (connection) info directly on our parent
  # and only if they are supposed to connect to us.
  $_is_this_direct_parent_of_node = ($this_zone_name == $node_zone_data['parent'])
  $_keep_host_attribute_parent_to_child = ($_is_this_direct_parent_of_node and !$node_agent_connects_to_parent)
  $_keep_host_attribute_child_to_parent = ($_is_this_direct_parent_of_node and $node_agent_connects_to_parent)
  $_node_endpoint_data_filtered = $node_endpoint_data.filter |$k, $v| {
    $k != 'host' or $_keep_host_attribute_parent_to_child
  }
  $_this_endpoint_data_filtered = $this_endpoint_data.filter |$k, $v| {
    $k != 'host' or $_keep_host_attribute_child_to_parent
  }

  # Create this endpoint and zone
  icinga2::object::endpoint{ $node_endpoint_name:
    * => $_node_endpoint_data_filtered,
  }
  icinga2::object::zone{ $node_zone_name:
    * => $node_zone_data,
  }

  # Collect information from descendants, creating endpoints and zone objects for all descendants
  Mon::Icinga2::Hierarchy::Node_and_subtree_exported <<| node_parent_zone == $node_zone_name |>> {
    this_endpoint_data => $this_endpoint_data,
    this_zone_data     => $this_zone_data,
  }

  # export this puppet node's data to the host that exported this resource
  if $node_endpoint_name != $this_endpoint_name { # not to self
    @@icinga2::object::endpoint{ "${this_endpoint_name}-to-${node_endpoint_name}":
      tag => "mon::icinga2::hierarchy::to-${node_endpoint_name}",
      *   => merge(
        $_this_endpoint_data_filtered,
        pick($mon::icinga2::hierarchy::exported_my_endpoint_overrides_for_peer[$node_endpoint_name], {}),
      ),
    }
    @@icinga2::object::zone{ "${this_zone_name}-to-${node_endpoint_name}":
      tag => "mon::icinga2::hierarchy::to-${node_endpoint_name}",
      *   => $this_zone_data,
    }
  }
}
