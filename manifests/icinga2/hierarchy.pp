# Set up zones and endpoints config on all icinga2 hosts.
#
# @param exported_my_endpoint_overrides_for_peer
#   Override values for setting in each given endpoint we export.  This
#   can be used to, for instance, redirect connections if host Alice
#   cannot get to its monitoring upstream Uncle via the normal route:
#   set this to { Alice => { host => 192.0.2.42 } } in Uncles's hiera.
#
#   That let's Alice know that to talk to Uncle, it should connect to 192.0.2.42.
class mon::icinga2::hierarchy (
  Hash[Stdlib::Fqdn, Hash] $exported_my_endpoint_overrides_for_peer = {}
) {
  assert_private()
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  $this_zone_name = $mon::this_zone_name
  if $mon::icinga2_is_master {
    $log_duration = undef
    $this_parent_zone = undef
  } else {
    $log_duration = 0
    $this_parent_zone = $mon::icinga2_parent_zone
  }

  $this_endpoint_name = $trusted['certname']

  $this_endpoint_data = {
    endpoint_name => $this_endpoint_name,
    host          => $this_endpoint_name,
    log_duration  => $log_duration,
  }
  $this_zone_data = {
    zone_name => $this_zone_name,
    endpoints => [ $this_endpoint_name ],
    parent    => $this_parent_zone,
  }

  @@mon::icinga2::hierarchy::node_and_subtree_exported { $this_endpoint_name:
    node_endpoint_data => $this_endpoint_data,
    node_zone_data     => $this_zone_data,
    this_endpoint_data => $this_endpoint_data,
    this_zone_data     => $this_zone_data,
  }

  ## Create zone and objects for this node and all our descendants
  ## and export ourselves to them
  Mon::Icinga2::Hierarchy::Node_and_subtree_exported <<| title == $this_endpoint_name |>>

  # collect endpoints and zones from the parent all the way up to the root
  Icinga2::Object::Endpoint <<| tag == "mon::icinga2::hierarchy::to-${this_endpoint_name}" |>>
  Icinga2::Object::Zone <<| tag == "mon::icinga2::hierarchy::to-${this_endpoint_name}" |>>
  Icinga2::Object::Zone <<| global == true |>>

  if $mon::icinga2_is_primary {
    @@icinga2::object::zone { 'global-templates':
      global => true,
    }
  }
}
