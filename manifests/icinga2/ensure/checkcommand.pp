# Ensure a check command definition exists on the icinga system
#
# This define is exported by various hosts and collected on the icinga master
# where it ensures that a resource for this check command exists.
#
# @param checkcommand_name        cf. icinga2::object::checkcommand
#   Defaults to title, or - if title has a / - the part before the /.
#   This enables callers to use
#     @@mon::icinga2::ensure::checkcommand { "Foo/$trusted['certname']": ..}
#   or something to that effect.
# @param command                  cf. icinga2::object::checkcommand
# @param env                      cf. icinga2::object::checkcommand
# @param vars                     cf. icinga2::object::checkcommand
# @param timeout                  cf. icinga2::object::checkcommand
# @param arguments                cf. icinga2::object::checkcommand
define mon::icinga2::ensure::checkcommand (
  String                              $checkcommand_name = if ('/' in $title) { $title.split('/')[0] } else { $title },
  Optional[Variant[Array, String]]    $command           = ["${mon::monitoring_home}/bin/${checkcommand_name}"],
  Optional[Hash]                      $env               = undef,
  Optional[Icinga2::CustomAttributes] $vars              = undef,
  Optional[Icinga2::Interval]         $timeout           = undef,
  Optional[Variant[Hash, String]]     $arguments         = undef,
) {
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }
  assert_private()

  ensure_resource( 'icinga2::object::checkcommand', $checkcommand_name, {
    target            => '/etc/icinga2/zones.d/global-templates/checkcommands.conf',

    checkcommand_name => $checkcommand_name,
    command           => $command,
    env               => $env,
    vars              => $vars,
    timeout           => $timeout,
    arguments         => $arguments,
  })
}
