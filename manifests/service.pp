# Add a service check for a generic icinga check command
#
# The resource title usually follows the pattern '<check_command>/<service_name>', but
# of course you are free to also set these two explicitly when it makes sense.
#
# The arguments that are supposed to be passed to a service check live in the vars
# hash.  They are type-checked against schemas stored in hiera such as this:
#
#     mon::icinga2_checkcommands:
#       swap:
#         mon_meta:
#           types:
#             allswaps: Optional[Boolean]
#             noswap: Optional[Enum['ok', 'warning', 'critical', 'unknown']]
#
# Any data not under non_meta is used by the mon class itself to create
# check_commands for icinga2.  Cf. mon.
#
# The following keys can live under mon_meta:
#  o install_command
#    Whether to force installing or not installing the check_command script.
#    By default, install_command is true if there are keys other than
#    mon_meta in this checkcommands hash, and so mon created the check_command
#    in icinga -- i.e., this check command is one that we are adding via puppet
#    and so it makes sense that we also roll out the script to perform the
#    actual check.
#  o on_target
#    Whether this check is supposed to run on the monitored system itself (if
#    true) or on the monitoring master (if false).  Defaults to true.  Can be
#    overridden in each instance of mon::service by passing an explicit
#    on_target value there.
#  o types
#    A dictioanry of acceptable variables/arguments for this check_command.
#    Maps from an argument name to a puppet type string.
#  o vars_key_prefix
#    A string to prefix to all variable/argument names before passing it on to icinga2.
#    By default, this is the check name followed by an underscore if not specified in
#    hiera, and again this can be overridden in each instance of mon::service
#    by passing an explicit value to vars_key_prefix.
#
# @param check_command
#   Icinga check command for this service
#   If this resource title contains a slash (/), then the default value is the part before
#   the slash, else the entire title.
# @param service_name
#   Name for this service
#   If this resource title contains a slash (/), then the default value is the part after
#   the slash, else the entire title.  In either case, an initial "check_" is removed.
# @param display_name
#   A short description for this service
# @param on_target
#   Run the service check locally on the target host that is being checked
# @param worker
#   Endpoint to run this service's check from, if not from the host itself
# @param host_name
#   Icinga host that this service belongs to
# @param vars
#   Variables (and their values) to set for this object
#   The final variable set is obtained by merging vars_default, vars_hiera*, and this,
#   where vars_hiera* may be provided in hiera under mon::service-defaults::<check_command>::vars.
#   and mon::service-defaults::<check_command>::<service_name>::vars.
# @param vars_default
#   Variables values set via puppet that can still be overridden by hiera values.
# @param vars_raw
#   Additional variables (and their values) to set for this object directly.
#   Variables here bypass the validation this module usually does and are taken
#   as is, without renaming based on vars_key_prefix.
#   This can be used to pass extra data to consumers other then the service check itself,
#   such as notification apply rules.
#
# @param validate_vars
#   Validate the keys of vars against mon::icinga2_checkcommands.
# @param install_command
#   Force installing (or not installing) the check_command script.  If not
#   provided, we use the default from the check_command definition in hiera, or
#   more precisely in mon::icinga2_checkcommands.
#   If we create the checkcommand (cf. create_check_command), this defaults to true
#   but can still be set to false explicitly.
# @param vars_key_prefix
#   Add this prefix to all keys in the vars hash.  Defaults to the data in hiera, or
#   check_command followed by an underscore.
# @param create_check_command
#   Implicitly create a check command.  This is primarily useful if we want to
#   create a command without defining it in hiera first.  Usually this is only
#   done for commands that take no arguments and probably only have one per host.
#   This will also try to install the check command unless install_command is explictly
#   set to false.
#   If this value is a hash, its arguments are passed to create the check
#   command (vars, args, etc), and any mon_meta values are used as if they were
#   also in hera.
#   We only validate vars if mon_meta has any types.
#
# @param check_interval      cf. icinga2::object::service
# @param retry_interval      cf. icinga2::object::service
# @param max_check_attempts  cf. icinga2::object::service
define mon::service (
  Pattern[/\A[a-zA-Z0-9_-]+\z/] $check_command = if ('/' in $title) { $title.split('/')[0] } else { $title },
  String                        $service_name  = $title.regsubst(/^(.*?\/)?(check_)?/, ''),
  Optional[String]              $display_name  = undef,
  Optional[Boolean]             $on_target     = undef,
  Optional[Stdlib::Host]        $worker        = undef,
  Optional[Stdlib::Host]        $host_name     = undef,
  Icinga2::CustomAttributes     $vars          = {},
  Icinga2::CustomAttributes     $vars_raw      = {},
  Icinga2::CustomAttributes     $vars_default  = {},
  Optional[String]              $vars_key_prefix = undef,

  Boolean                       $validate_vars = true,
  Optional[Boolean]             $install_command = undef,
  Variant[Boolean, Hash]        $create_check_command = false,

  Optional[Icinga2::Interval]   $check_interval = undef,
  Optional[Icinga2::Interval]   $retry_interval = undef,
  Optional[Integer[1]]          $max_check_attempts = undef,
) {
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }
  $mon_meta = pick(
    $mon::icinga2_checkcommands.dig($check_command, 'mon_meta'),
    if $create_check_command =~ Hash { $create_check_command['mon_meta'] } else { undef },
    {},
  )
  if pick($mon_meta['internal'], false) {
    assert_private()
  }

  $vars_hiera_command = lookup(
    "mon::service-defaults::${check_command}::vars",
    Icinga2::CustomAttributes,
    {
      'strategy'        => 'deep',
      'knockout_prefix' => '--',
    },
    {}
  )
  $vars_hiera_service = lookup(
    "mon::service-defaults::${check_command}::${service_name}::vars",
    Icinga2::CustomAttributes,
    {
      'strategy'        => 'deep',
      'knockout_prefix' => '--',
    },
    {}
  )
  $_vars = merge(
    $vars_default.delete_undef_values(),
    $vars_hiera_command.delete_undef_values(),
    $vars_hiera_service.delete_undef_values(),
    $vars.delete_undef_values(),
  )
  if $create_check_command {
    $_filtered_data = if $create_check_command =~ Hash {
      $create_check_command.filter |$key, $val| { $key != 'mon_meta' }
    } else {
      {}
    }
    @@mon::icinga2::ensure::checkcommand { "${check_command}/${trusted['certname']}" :
      * => $_filtered_data,
    }
    $force_skip_validation = ($mon_meta['types'] =~ Undef)
  } else {
    $force_skip_validation = false
  }
  if $validate_vars and !$force_skip_validation{
    $check_prototype_types = $mon_meta['types']
    if $check_prototype_types =~ Undef {
      fail("Did not find check_command ${check_command} or types for it in mon::icinga2_checkcommands.")
    }

    $check_prototype_types.each |$param_name, $param_type| {
      assert_type($param_type, $_vars[$param_name]) |$expected, $actual| {
        fail("Check ${title}: Invalid type for variable ${param_name}.  Expected '${expected}', got '${actual}'.")
      }
    }
    $invalid_keys = $_vars.keys() - $check_prototype_types.keys()
    unless $invalid_keys.empty() {
      fail("Check ${title}: Invalid variables: ${invalid_keys}.")
    }
  }

  $_on_target_from_checkcommand = $mon_meta['on_target']
  $_on_target = pick($on_target, $_on_target_from_checkcommand, true)

  # the reduce in the second line is like pick, but also accepts empty strings
  $_vars_key_prefix_from_checkcommand = $mon_meta['vars_key_prefix']
  $_vars_key_prefix = [$vars_key_prefix, $_vars_key_prefix_from_checkcommand, "${check_command}_"].reduce |$memo, $value| {
    [$memo, $value][Integer($memo =~ Undef)]
  }

  $_vars_renamed = Hash($_vars.delete_undef_values().map |$key, $value| {
    [ "${_vars_key_prefix}${key}", $value ]
  })

  mon::icinga2::object::service { "mon::service::${title}" :
    check_command      => $check_command,
    service_name       => $service_name,
    display_name       => $display_name,
    on_target          => $_on_target,
    worker             => $worker,
    host_name          => $host_name,
    vars               => $_vars_renamed + pick($vars_raw, {}),

    check_interval     => $check_interval,
    retry_interval     => $retry_interval,
    max_check_attempts => $max_check_attempts,
  }

  $_install_command_from_checkcommand = $mon_meta['install_command']
  $_install_command_default = ($check_command in $mon::icinga2_checkcommands) and
                              ! ($mon::icinga2_checkcommands[$check_command].keys() - ['mon_meta']).empty()
  $_install_command = pick(
    $install_command,
    if $create_check_command { true } else { undef },
    $_install_command_from_checkcommand,
    $_install_command_default)
  if $_install_command {
    if $worker {
      mon::install_command_request { "mon::service[${title}]" :
        worker       => $worker,
        command_name => $check_command,
      }
    } elsif $_on_target or $mon::icinga2_is_master {
      mon::install_command { "mon::service[${title}]" :
        command_name => $check_command,
      }
    } else {
      # install check command on master nodes
      $mon::icinga2_masters.each |$master| {
        mon::install_command_request { "mon::service[${title}]/${master}" :
          worker       => $master,
          command_name => $check_command,
        }
      }
    }
  }
}
