# Install a check or notification script on a host
#
# The check or notification script name is given by command_name, which
# defaults to the resource title.
#
# If a class by that name exists in any of the mon::checkcommand_classname_prefixes,
# then we include that class.  Otherwise, we try to install the file in
# $mon::monitoring_home/bin/ from the sources in mon::checkcommand_sources.
#
# @param command_name
#   Icinga check command for this service
# @param include_check_class
#   Attempt to include the check class first, if it exists.
#   If set to false, will only install the check file, even if a class exists.
#   True by default unless source is provided.
# @param source
#   Optional puppet source for creating the check command.
#   By default, we search mon::all_checkcommand_sources for the appropriate script.
define mon::install_command (
  Pattern[/\A[a-zA-Z0-9_]+\z/] $command_name = $title,
  Optional[String] $source = undef,
  Boolean $include_check_class = ($source =~ Undef),
) {
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  if $include_check_class {
    $potential_class_names = $mon::all_checkcommand_classname_prefixes.map |$prefix| {
      "${prefix}::${command_name}"
    }
    $defined_class_names = $potential_class_names.filter |$cn| { defined($cn) }
  }

  if $include_check_class and ! $defined_class_names.empty() {
    include $defined_class_names[0]
  } else {
    $potential_source_names = $mon::all_checkcommand_sources.map |$prefix| {
      "${prefix}/${command_name}"
    }
    ensure_resource( 'file', "${mon::monitoring_home}/bin/${command_name}", {
      mode   => '0555',
      owner  => 'root',
      group  => 'root',
      source => pick($source, $potential_source_names),
    })
  }
}
