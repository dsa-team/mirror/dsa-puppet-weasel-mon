# Monitor basic services on our hosts.
#
# @param addresses List of addresses for this host.
#   Override addresses list.
# @param host_check_options
#   Optional options to give to the host check
# @param ssh_check       Add a ssh check for this host.
# @param swap_check      Add a swap check for this host.
class mon::services (
  Optional[Array[Stdlib::IP::Address::Nosubnet]] $addresses = undef,
  Hash    $host_check_options = {},

  Boolean $ssh_check = true,
  Boolean $swap_check = true,
) {
  mon::icinga2::object::host { $trusted['certname']:
    addresses => $addresses,
    *         => $host_check_options,
  }

  unless $mon::icinga2_is_master {
    mon::service { 'cluster-zone':
      display_name => 'icinga2-cluster-zone-health',
      vars         => {
        cluster_zone => $mon::this_zone_name,
      }
    }
  }

  if $mon::icinga2_is_master {
    icinga2::object::dependency { 'dependency-agent-health-check':
      parent_host_name    => 'service.command_endpoint',
      parent_service_name => 'cluster-zone',
      ignore_soft_states  => false,
      disable_checks      => true,
      states              => ['OK'],
      apply               => true,
      apply_target        => 'Service',
      assign              => ['service.command_endpoint',],
      ignore              => ['service.name == cluster-zone',] + $mon::icinga2_masters.map |$m| { "service.command_endpoint == ${m}" },
      target              => '/etc/icinga2/zones.d/global-templates/apply-dependencies.conf',
    }
  }

  include mon::service::disk_all
  include mon::service::ping

  mon::service { 'icinga': }
  mon::service { 'load': }
  mon::service { 'procs': }
  mon::service { 'procs/procs-zombies':
    vars_default => {
      state    => 'Z',
      warning  => 10,
      critical => 20,
    }
  }
  if $ssh_check {
    mon::service { 'ssh': }
  }
  if $swap_check {
    mon::service { 'swap': }
  }
  mon::service { 'users': }
}
