# Set up and configure icinga2 itself on all devices.
class mon::icinga2 (
) {
  assert_private()
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  # This is used by the mon_icinga2_source_addresses facter
  mon::misc::add_info_fact { 'mon::icinga2::icinga2_masters':
    data => {
      'icinga2_masters' => $mon::icinga2_masters,
    },
  }

  class{ 'mon::icinga2::firewall':
  }

  file { [
    '/etc/icinga2/conf.d',
    '/etc/icinga2/zones.d',
    ]:
    ensure  => directory,
    force   => true,
    recurse => true,
    purge   => true,
    owner   => 'root',
    group   => 'root',
    mode    => '0755'
  }

  if $mon::icinga2_is_master {
    class{ 'mon::icinga2::master':
    }
  } else {
    class{ 'mon::icinga2::agent':
    }
  }
  class{ 'mon::icinga2::hierarchy':
  }
}
