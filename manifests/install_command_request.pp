# Request that a (check) command script be installed on the worker
#
# @param command_name
#   Icinga check command for this service
# @param worker
#   Host to install check command on
define mon::install_command_request (
  Stdlib::Host $worker,
  Pattern[/\A[a-zA-Z0-9_]+\z/] $command_name = $title,
) {
  if ! defined(Class['mon']) {
    fail('You must include the mon base class first.')
  }

  @@mon::install_command { "${trusted['certname']}--${title}":
    tag          => "worker--${worker}",
    command_name => $command_name,
  }
}
