# Put some facts onto the host that can then be used by other classes or
# types to adapt their monitoring behavior.
#
# @param data
#   Hash of data to add as a mon::misc fact.
define mon::misc::add_info_fact (
  Hash $data
) {
  ['/etc/facter', '/etc/facter/facts.d'].each |$dir| {
    ensure_resource('file', $dir, {
      ensure => directory,
    })
  }

  ensure_resource('concat', 'mon::misc::info_facts', {
    path   => '/etc/facter/facts.d/mon-misc-info-facts.json',
    format => 'json-pretty',
  })

  $facts_hash = {
    'mon' => {
      'misc' => $data,
    },
  }
  concat::fragment { "mon::misc::add_info_fact/${title}":
    target  => 'mon::misc::info_facts',
    content => inline_template( @(EOF) ),
        <%= require 'json'; @facts_hash.to_json %>
        | EOF
  }
}
