# Check one filesystem, and add that file system to the exclude list for disk_all.
#
# @param service_name
#   Name of this icinga2 service
# @param display_name
#   A short description of this service
#
# @param wfree
#   Exit with WARNING status if less than INTEGER units of disk are free or Exit with WARNING status if less than PERCENT of disk space is free
# @param cfree
#   Exit with CRITICAL status if less than INTEGER units of disk are free or Exit with CRITCAL status if less than PERCENT of disk space is free
#
# @param partitions
#   Path or partition (may be repeated), defaults to the resource title.
# @param include_type
#   Check only filesystems of indicated type (may be repeated)
# @param ereg_path
#   Regular expression for path or partition (may be repeated)
# @param eregi_path
#   Case insensitive regular expression for path/partition (may be repeated)
#
# @param inode_cfree
#   Exit with CRITICAL status if less than PERCENT of inode space is free
# @param inode_wfree
#   Exit with WARNING status if less than PERCENT of inode space is free
#
# @param units
#   Choose bytes, kB, MB, GB, TB (default: MB)
#
# @param errors_only
#   Display only devices/mountpoints with errors
#
# @param ignore_in_disk_all
#   Automatically add the filesystem mount point or type or regex to the list of
#   elements ignored by the disk-all check.
#
# @param all
#   Explicitly select all paths.
#   You should not use this directly; instead go through mon::service::disk_all.
# @param exclude_type
#   Filesystem types to ignore when checking.  Cf. disk check options.
# @param ignore_ereg_path
#   Paths by regex to ignore when checking.  Cf. disk check options.
# @param ignore_eregi_path
#   Paths by case-insensitive regex to ignore when checking.  Cf. disk check options.
# @param partitions_excluded
#   Exclude these mountpoints from checking.  Cf. disk check options.
#
# @param sudo
#   Run check as root or as the given user.  If this is provided, we have to
#   build our own check_disk command line instead of relying on icinga's.
define mon::service::disk (
  String $service_name                                                            = "disk ${title}",
  Optional[String] $display_name                                                  = undef,

  Optional[Variant[Integer,Pattern[/\A[0-9]+%\z/]]] $wfree                        = undef,
  Optional[Variant[Integer,Pattern[/\A[0-9]+%\z/]]] $cfree                        = undef,

  Optional[Variant[Stdlib::Absolutepath,Array[Stdlib::Absolutepath]]] $partitions = if $title =~ Stdlib::Absolutepath { $title } else { undef },
  Optional[Variant[String,Array[String]]] $include_type                           = undef,
  Optional[Variant[String,Array[String]]] $ereg_path                              = undef,
  Optional[Variant[String,Array[String]]] $eregi_path                             = undef,

  Optional[Enum['bytes', 'kB', 'MB', 'GB', 'TB']] $units                          = undef,
  Optional[Integer[0,100]] $inode_wfree                                           = if $wfree =~ Pattern[/\A[0-9]+%\z/] { Integer($wfree.regsubst('%','')) } else { undef },
  Optional[Integer[0,100]] $inode_cfree                                           = if $cfree =~ Pattern[/\A[0-9]+%\z/] { Integer($cfree.regsubst('%','')) } else { undef },

  Array[String] $exclude_type                      = [],
  Array[String] $ignore_ereg_path                  = [],
  Array[String] $ignore_eregi_path                 = [],
  Array[Stdlib::AbsolutePath] $partitions_excluded = [],

  Boolean $ignore_in_disk_all                                                     = true,
  Boolean $errors_only =
    $include_type !~ Undef or $ereg_path !~ Undef or $eregi_path != Undef or Array($partitions, true).delete_undef_values().length() != 1,

  Boolean $all = false,

  Variant[Boolean,Pattern[/\A[a-z0-9_][a-z0-9_-]*\z/]] $sudo = false,
) {
  $num_defined_filters = [$title, $include_type, $ereg_path, $eregi_path].delete_undef_values().length()
  if $num_defined_filters == 0 {
    fail('Need to specify a partition(mountpoint), a type, or an (i)regex for a path.')
  } elsif $num_defined_filters == 1 and $ignore_in_disk_all {

    $_ignore_hash = {
      'partitions' => Array($partitions  , true).delete_undef_values(),
      'type'       => Array($include_type, true).delete_undef_values(),
      'ereg_path'  => Array($ereg_path   , true).delete_undef_values(),
      'eregi_path' => Array($eregi_path  , true).delete_undef_values(),
    }.filter |$key, $val| {
      ! $val.empty()
    }

    unless $_ignore_hash.empty() {
      mon::misc::add_info_fact { "mon::service::disk/${title}":
        data => {
          'disk_all' => {
            'ignore' => $_ignore_hash,
          },
        },
      }
    }
  }

  unless $sudo {
    mon::service { "disk/${title}" :
      service_name => $service_name,
      display_name => $display_name,
      vars         => {
        all                 => $all,
        cfree               => $cfree,
        ereg_path           => $ereg_path,
        eregi_path          => $eregi_path,
        errors_only         => $errors_only,
        exact_match         => true,
        exclude_type        => $exclude_type,
        ignore_ereg_path    => $ignore_ereg_path,
        ignore_eregi_path   => $ignore_eregi_path,
        inode_cfree         => $inode_cfree,
        inode_wfree         => $inode_wfree,
        partitions          => $partitions,
        partitions_excluded => $partitions_excluded,
        units               => $units,
        wfree               => $wfree,
      }.filter |$key, $val| {
        $val !~ Array[Any,0,0]
      }
    }
  } else {
    $command = [
      '/usr/lib/nagios/plugins/check_disk',
    ] +
    if $wfree               =~ Undef { [] } else { [ '-w', $wfree ] } +
    if $cfree               =~ Undef { [] } else { [ '-c', $cfree ] } +
    if $inode_wfree         =~ Undef { [] } else { [ '-W', $inode_wfree ] } +
    if $inode_cfree         =~ Undef { [] } else { [ '-K', $inode_cfree ] } +

    if $units               =~ Undef { [] } else { [ '-u', $units ] } +

    if $include_type        =~ Undef { [] } else { Array($include_type       , true).reduce([]) |$memo, $value| { $memo + [ '-N', $value ] } } +
    if $exclude_type        =~ Undef { [] } else { Array($exclude_type       , true).reduce([]) |$memo, $value| { $memo + [ '-X', $value ] } } +
    if $partitions_excluded =~ Undef { [] } else { Array($partitions_excluded, true).reduce([]) |$memo, $value| { $memo + [ '-x', $value ] } } +
    if $ereg_path           =~ Undef { [] } else { Array($ereg_path          , true).reduce([]) |$memo, $value| { $memo + [ '-r', $value ] } } +
    if $eregi_path          =~ Undef { [] } else { Array($eregi_path         , true).reduce([]) |$memo, $value| { $memo + [ '-R', $value ] } } +

    [ '-E' ] + # exact match
    if $all                 =~ Undef { [] } else { ['-A'] } +
    if $errors_only         =~ Undef { [] } else { ['-e'] } +
    if $partitions          =~ Undef { [] } else { Array($partitions         , true).reduce([]) |$memo, $value| { $memo + [ '-p', $value ] } } +

    if $ignore_ereg_path    =~ Undef { [] } else { Array($ignore_ereg_path   , true).reduce([]) |$memo, $value| { $memo + [ '-i', $value ] } } +
    if $ignore_eregi_path   =~ Undef { [] } else { Array($ignore_eregi_path  , true).reduce([]) |$memo, $value| { $memo + [ '-I', $value ] } } +
    []

    $command_str = $command.map |$x| { String($x) }
    if $command_str.any |$x| { $x =~ /[']/ } {
      fail('Invalid characters in disk_free check argument.')
    }

    mon::service::shell_wrapped { "disk ${title}":
      service_name => $service_name,
      display_name => $display_name,
      sudo         => $sudo,
      command      => $command_str.map |$x| { "'${x}'" }.join(' '),
    }
  }
}
