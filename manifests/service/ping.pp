# Set up ping checks for all the addresses of this system
#
# @param cpl
#   Percentage of packet loss to trigger an alarm state
# @param crta
#   round trip average travel time (ms) which triggers a CRITICAL state
# @param packets
#   number of ICMP ECHO packets to send (Default: 5)
# @param timeout
#   Seconds before connection times out (default: 10)
# @param wpl
#   Percentage of packet loss to trigger an alarm state
# @param wrta
#   round trip average travel time (ms) which triggers a WARNING state
#
# @param rfc1918_addresses
#   Add ping checks for IPv4 Private-Use (RFC1918) addresses
# @param exclude_addresses
#   Exclude addresses in these networks from ping checks
class mon::service::ping (
  Optional[Integer[0,100]] $cpl = undef,
  Optional[Integer] $crta       = undef,
  Optional[Integer] $packets    = undef,
  Optional[Integer] $timeout    = undef,
  Optional[Integer[0,100]] $wpl = undef,
  Optional[Integer] $wrta       = undef,
  Boolean $rfc1918_addresses = false,
  Array[Stdlib::IP::Address] $exclude_addresses = [],
) {
  $my_addresses = $facts['networking']['interfaces'].map |$if, $data| {
    pick($data['bindings'],[]).map |$binding| {
      $binding['address']
    } +
    pick($data['bindings6'],[]).map |$binding| {
      # old debian.org puppet does not have that
      #if $binding['scope6'] == 'global' {
        $binding['address']
      #}
    }
  }.flatten().delete_undef_values().unique().filter |$addr| {
    true
    and ! ip_in_cidr($addr, '127.0.0.0/8') # IPv4 localhost [RFC6890]
    and ! ip_in_cidr($addr, '::1/128')     # IPv6 localhost [RFC6890]
    and ! ip_in_cidr($addr, '169.254.0.0/16') # IPv4 link local [RFC6890]
    and ! ip_in_cidr($addr, 'fe80::/10')      # IPv6 link local [RFC6890]
    and ($rfc1918_addresses or ! ip_in_cidr($addr, '10.0.0.0/8'))     # IPv4 Private-Use [RFC1968; RFC6890]
    and ($rfc1918_addresses or ! ip_in_cidr($addr, '172.16.0.0/12'))  # IPv4 Private-Use [RFC1968; RFC6890]
    and ($rfc1918_addresses or ! ip_in_cidr($addr, '192.168.0.0/16')) # IPv4 Private-Use [RFC1968; RFC6890]
    and ! $exclude_addresses.any |$excluded_net| { ip_in_cidr($addr, $excluded_net) } # Explicit exclude
  }

  $my_addresses.each |$address| {
    mon::service { "ping/ping ${address}":
      vars => {
        address => $address,
        cpl     => $cpl,
        crta    => $crta,
        packets => $packets,
        timeout => $timeout,
        wpl     => $wpl,
        wrta    => $wrta,
      }
    }
  }
}
