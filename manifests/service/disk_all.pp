# Check all filesystems
#
# Auto-ignore filesystems from mon::service::disk.
#
# @param wfree
#   Exit with WARNING status if less than INTEGER units of disk are free or Exit with WARNING status if less than PERCENT of disk space is free
# @param cfree
#   Exit with CRITICAL status if less than INTEGER units of disk are free or Exit with CRITCAL status if less than PERCENT of disk space is free
#
# @param inode_cfree
#   Exit with CRITICAL status if less than PERCENT of inode space is free
# @param inode_wfree
#   Exit with WARNING status if less than PERCENT of inode space is free
#
# @param units
#   Choose bytes, kB, MB, GB, TB (default: MB)
# @param exclude_type
#   Filesystem types to ignore when checking.  Cf. disk check options.
# @param ignore_ereg_path
#   Paths by regex to ignore when checking.  Cf. disk check options.
# @param ignore_eregi_path
#   Paths by case-insensitive regex to ignore when checking.  Cf. disk check options.
# @param partitions_excluded
#   Exclude these mountpoints from checking.  Cf. disk check options.
#
# @param sudo
#   Run check as root or as the given user.
class mon::service::disk_all (
  Variant[Integer,Pattern[/\A[0-9]+%\z/]] $wfree          = '20%',
  Variant[Integer,Pattern[/\A[0-9]+%\z/]] $cfree          = '10%',

  Optional[Enum['bytes', 'kB', 'MB', 'GB', 'TB']] $units  = undef,
  Integer[0,100] $inode_wfree                             = if $wfree =~ Pattern[/\A[0-9]+%\z/] { Integer($wfree.regsubst('%','')) } else { 20 },
  Integer[0,100] $inode_cfree                             = if $cfree =~ Pattern[/\A[0-9]+%\z/] { Integer($cfree.regsubst('%','')) } else { 10 },

  Array[String] $exclude_type = [],
  Array[String] $ignore_ereg_path = [],
  Array[String] $ignore_eregi_path = [],
  Array[Stdlib::AbsolutePath] $partitions_excluded = [],

  Variant[Boolean,Pattern[/\A[a-z0-9_][a-z0-9_-]*\z/]] $sudo = false,
) {
  $exclude_types_base = [
    'cgroup',
    'configfs',
    'debugfs',
    'devpts',
    'devtmpfs',
    'hugetlbfs',
    'overlay',
    'proc',
    'shm',
    'sysfs',
    'tracefs',
    'udev',
  ]

  $fact_exclude_type        = pick($facts.dig('mon', 'misc', 'disk_all', 'ignore', 'type'), [])
  $fact_ignore_ereg_path    = pick($facts.dig('mon', 'misc', 'disk_all', 'ignore', 'ereg_path'), [])
  $fact_ignore_eregi_path   = pick($facts.dig('mon', 'misc', 'disk_all', 'ignore', 'eregi_path'), [])
  $fact_partitions_excluded = pick($facts.dig('mon', 'misc', 'disk_all', 'ignore', 'partitions'), [])

  $_exclude_type        = ($exclude_type        + $fact_exclude_type + $exclude_types_base).unique()
  $_ignore_ereg_path    = ($ignore_ereg_path    + $fact_ignore_ereg_path).unique()
  $_ignore_eregi_path   = ($ignore_eregi_path   + $fact_ignore_eregi_path).unique()
  $_partitions_excluded = ($partitions_excluded + $fact_partitions_excluded).unique()

  mon::service::disk { 'all':
    service_name        => 'disk all',
    sudo                => $sudo,

    all                 => true,
    partitions          => [],
    cfree               => $cfree,
    wfree               => $wfree,
    inode_cfree         => $inode_cfree,
    inode_wfree         => $inode_wfree,

    errors_only         => true,
    units               => $units,

    exclude_type        => $_exclude_type,
    ignore_ereg_path    => $_ignore_ereg_path,
    ignore_eregi_path   => $_ignore_eregi_path,
    partitions_excluded => $_partitions_excluded,
  }
}
