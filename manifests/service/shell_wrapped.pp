# Call an arbitrary, puppet defined, check command on a target
#
# @param service_name
#   Name of this icinga2 service
# @param display_name
#   A short description of this service
#
# @param command
#   Shell command or script to run
# @param sudo
#   Run shell command as root or as the given user
# @param script_name
#   Name of the wrapper script we produce (defaults to service name)
# @param install_commands    Optional hash of check scripts to install (instances of mon::install_command)
#
# @param host_name           cf. icinga2::object::service
# @param check_interval      cf. icinga2::object::service
# @param retry_interval      cf. icinga2::object::service
# @param max_check_attempts  cf. icinga2::object::service
# @param vars_raw            cf. mon::service
define mon::service::shell_wrapped (
  String $command,
  String $service_name = $title,
  Pattern[/\A[a-zA-Z0-9_][a-zA-Z0-9 _.-]*\z/] $script_name = $title.regsubst('[^a-zA-Z0-9_-]', '_', 'G'),


  Optional[String] $display_name = undef,
  Variant[Boolean,Pattern[/\A[a-z0-9_][a-z0-9_-]*\z/]] $sudo = false,
  Optional[Hash[String,Hash]] $install_commands   = undef,

  Optional[Stdlib::Host]      $host_name          = undef,
  Optional[Icinga2::Interval] $check_interval     = undef,
  Optional[Icinga2::Interval] $retry_interval     = undef,
  Optional[Integer[1]]        $max_check_attempts = undef,
  Optional[Icinga2::CustomAttributes] $vars_raw   = undef,
) {
  include mon

  ensure_resource( 'file', "${mon::monitoring_home}/etc/shell_wrapped", {
    ensure  => directory,
    force   => true,
    recurse => true,
    purge   => true,
    owner   => 'root',
    group   => 'root',
    mode    => '0711',
  })

  $full_check_command = "${mon::monitoring_home}/etc/shell_wrapped/${script_name}"
  file { $full_check_command:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => "#!/bin/sh\n\n${command}\n",
  }

  if $sudo =~ String {
    $sudo_user = $sudo
  } elsif $sudo {
    $sudo_user = 'root'
  } else {
    $sudo_user = undef
  }

  if $sudo_user {
    sudo::conf { "icinga-shell-${title}":
      content => "nagios ALL=(${sudo_user}) NOPASSWD: ${full_check_command}",
    }
  }

  mon::service { "shell_wrapped/${title}" :
    service_name       => $service_name,
    display_name       => $display_name,
    vars               => {
      command   => $script_name,
      sudo_user => $sudo_user,
    },
    vars_raw           => $vars_raw,
    host_name          => $host_name,
    check_interval     => $check_interval,
    retry_interval     => $retry_interval,
    max_check_attempts => $max_check_attempts,

    install_command    => false,
  }
  mon::install_command { "mon::service::shell_wrapped[${title}]" :
    command_name        => 'shell_wrapped',
    include_check_class => false,
  }
  if $install_commands {
    ensure_resources( 'mon::install_command',
      $install_commands,
      { include_check_class => false, }
    )
  }
}
